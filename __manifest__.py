# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{

    'name': 'Modulos Ayurdevas',

    'version': '1.0',

    'category': '',

    'summary': 'Lista de dependencias',

    'author': 'Multidevas SA',

    'website': 'ayurdevas.com',

    'depends': [
        'connector_ecommerce_ayurdevas',
        'pos_automatic_invoicing',
        'pos_accented_search',
        'pos_access_right',
        'pos_default_empty_image',
        'pos_set_default_customer',
        'pos_payment_change',
        'pos_sale_invoice',
        'pos_sales_campaign_invoice',
        'l10n_ar_point_of_sale_pos_ar',
        'points_miles_custom',
        'sale_order_type',
        'sale_disable_inventory_check',
        'stock_no_negative',
        'stock_picking_sale_order_link',
        'stock_available_immediately',
        'web_advanced_search',
        'web_decimal_numpad_dot',
        'product_pricelist_direct_print',
        'product_pricelist_revision',
        'partner_ref_unique',
        'ayurdevas_ux',
        'web_m2x_options',
    ],

    'data': [],

    'installable': True,

    'auto_install': False,

    'application': True,

    'description': 'Lista de dependencias',

}
