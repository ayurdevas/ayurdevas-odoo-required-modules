#Modulos necesarios para Odoo


##Aplicaciones a medida:

* [Ayurdevas Ecommerce Connector](https://gitlab.com/ayurdevas/connector-ecommerce-ayurdevas)
* [POS Automatic Invoicing](https://github.com/r-sierra/pos_automatic_invoicing)
* [Points & Miles Custom](https://gitlab.com/ayurdevas/points-and-miles-custom)
* [Terminal Punto de Venta - Talonario por defecto](https://github.com/r-sierra/l10n_ar_point_of_sale_pos_ar)
* [Ayurdevas UX](https://github.com/ayurdevas/ayurdevas-ux)
* [POS Sale Invoice](https://github.com/ayurdevas/pos-sale-invoice)
* [POS Sales Campaign Invoice](https://github.com/ayurdevas/pos-sales-campaign-invoice)


##OCA - Odoo Community Association:

* [POS Accented Search](https://github.com/oca/pos)
* [POS Access Right](https://github.com/oca/pos)
* [POS Default Empty Image](https://github.com/oca/pos)
* [Point Of Sale - Invoicing](https://github.com/OCA/pos/tree/12.0/pos_invoicing)
* [Point Of Sale - Change Payments](https://github.com/OCA/pos/tree/12.0/pos_payment_change)
* [Sale Order Type](https://github.com/oca/sale-workflow)
* [Sale Disable Inventory Check](https://github.com/OCA/sale-workflow)
* [Stock Disallow Negative](https://github.com/OCA/stock-logistics-workflow)
* [Stock Picking Sale Order Link](https://github.com/OCA/stock-logistics-workflow)
* [Stock Available Immediately](https://github.com/OCA/stock-logistics-warehouse/tree/12.0/stock_available_immediately)
* [Advanced search](https://github.com/OCA/web)
* [Web - Numpad Dot as decimal separator](https://github.com/OCA/web)
* [web_m2x_options](https://github.com/OCA/web/tree/12.0/web_m2x_options)
* [Product Pricelist Direct Print](https://github.com/OCA/product-attribute)
* [Product Pricelist Revision](https://github.com/OCA/product-attribute/tree/12.0/product_pricelist_revision)
* [Product Cost Security](https://github.com/OCA/product-attribute/tree/12.0/product_cost_security)
* [Partner unique reference](https://github.com/oca/partner-contact)


##Aplicaciones de terceros:

* [POS Default Customer](https://apps.odoo.com/apps/modules/12.0/pos_set_default_customer/)


##Configuraciones
###web_m2x_options
En Ajustes\Técnico\Parámetros\Parámetros del sistema definir los siguientes valores:

* web_m2x_options.create: False
* web_m2x_options.create_edit: True
* web_m2x_options.search_more: True

###Product Cost Security
Agrega un grupo de seguridad para ver campo Costo del producto. Solo usuarios con
este permiso pueden ver el campo

Para usar este módulo se necesita:

    1. Ir a Ajustes\Usuarios y compañias\Usuarios.
    2. Elegir un usuario y seleccionar el grupo "Acceso a ver el coste de productos".

##Cantidad Disponible Inmediato
Este modulo propone varias formas de calcular la cantidad prevista para cada producto. La cantidad
se basa en la cantidad prevista ṕero sin tener en cuenta las recepciones pendientes.

